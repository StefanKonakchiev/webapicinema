﻿using CinemAPI.Models.Contracts.Reservation;
using System.Threading.Tasks;

namespace CinemAPI.Data
{
    public interface IReservationTicketRepository
    {
        Task<bool> InsertAsync(IReservationTicketCreation reservationTicket);

        Task<bool> VerifySeatsAvailability(long projectionId, short row, short column);

        Task<IReservationTicketViewModel> Get(long reservationId);

        Task<IReservationTicketViewModel> Get(long projectionId, short row, short column);

        Task<bool> CheckIfSold(long reservationId);

        Task<bool> UpdateReservationStatus(long reservationId);
    }
}
