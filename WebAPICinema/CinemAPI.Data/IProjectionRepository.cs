﻿using CinemAPI.Models.Contracts.Projection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CinemAPI.Data
{
    public interface IProjectionRepository
    {
        IProjection Get(int movieId, int roomId, DateTime startDate);

        Task<IProjection> Get(long projectId);

        Task<IProjection> GetByReservation(long reservationId);

        Task<bool> CheckIfProjectionPriorToStart(long projectId);

        Task<bool> CheckIfProjectionStarted(long projectId);

        Task<bool> CheckIfProjectionHasFinished(long projectId);

        Task CancelActiveReservations(long projectId);

        Task IncreaseSeatAvailability(long projectId);

        Task DecreaseSeatAvailability(long projectId);

        void Insert(IProjectionCreation projection);

        IEnumerable<IProjection> GetActiveProjections(int roomId);
    }
}