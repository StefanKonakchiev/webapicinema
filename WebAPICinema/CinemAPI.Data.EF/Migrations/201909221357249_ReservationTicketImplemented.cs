namespace CinemAPI.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReservationTicketImplemented : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReservationTickets",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProjectionId = c.Long(nullable: false),
                        Row = c.Short(nullable: false),
                        Column = c.Short(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Projections", t => t.ProjectionId, cascadeDelete: true)
                .Index(t => t.ProjectionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReservationTickets", "ProjectionId", "dbo.Projections");
            DropIndex("dbo.ReservationTickets", new[] { "ProjectionId" });
            DropTable("dbo.ReservationTickets");
        }
    }
}
