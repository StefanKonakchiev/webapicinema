﻿using CinemAPI.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace CinemAPI.Data.EF.ModelConfigurations
{
    internal sealed class ReservationTicketModelConfiguration : IModelConfiguration
    {
        public void Configure(DbModelBuilder modelBuilder)
        {
            EntityTypeConfiguration<ReservationTicket> projectionModel = modelBuilder.Entity<ReservationTicket>();
            projectionModel.HasKey(model => model.Id);
            projectionModel.Property(model => model.ProjectionId).IsRequired();
            projectionModel.Property(model => model.Row).IsRequired();
            projectionModel.Property(model => model.Column).IsRequired();
            projectionModel.Property(model => model.Status).IsRequired();
        }
    }
}
