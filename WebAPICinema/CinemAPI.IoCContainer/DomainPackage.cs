﻿using CinemAPI.Domain;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.NewIssueTicket;
using CinemAPI.Domain.NewProjection;
using CinemAPI.Domain.NewReservationTicket;
using CinemAPI.Domain.NewRoom;
using CinemAPI.Domain.SeatsAvailability;
using SimpleInjector;
using SimpleInjector.Packaging;

namespace CinemAPI.IoCContainer
{
    public class DomainPackage : IPackage
    {
        public void RegisterServices(Container container)
        {
            container.Register<INewProjection, NewProjectionCreation>();
            container.RegisterDecorator<INewProjection, NewProjectionMovieValidation>();
            container.RegisterDecorator<INewProjection, NewProjectionUniqueValidation>();
            container.RegisterDecorator<INewProjection, NewProjectionRoomValidation>();
            container.RegisterDecorator<INewProjection, NewProjectionPreviousOverlapValidation>();
            container.RegisterDecorator<INewProjection, NewProjectionNextOverlapValidation>();

            container.Register<INewReservationTicket, NewReservationTicketCreation>();
            container.RegisterDecorator<INewReservationTicket, NewReservationTicketReservedSeatsValidation>();
            container.RegisterDecorator<INewReservationTicket, NewReservationTicketPriorToStartProjectionValidation>();
            container.RegisterDecorator<INewReservationTicket, NewReservationTicketStartedProjectionValidation>();
            container.RegisterDecorator<INewReservationTicket, NewReservationTicketFinishedProjectionValidation>();
            container.RegisterDecorator<INewReservationTicket, NewReservationTicketReservationsCancelValidation>();
            container.RegisterDecorator<INewReservationTicket, NewReservationTicketRoomSeatsValidation>();
            container.RegisterDecorator<INewReservationTicket, NewReservationTicketProjectionNonExistingValidation>();

            container.Register<INewIssueTicket, NewIssueTicketCreation>();
            container.RegisterDecorator<INewIssueTicket, NewIssueTicketSoldValidation>();
            container.RegisterDecorator<INewIssueTicket, NewIssueTicketPriorToStartProjectionValidation>();
            container.RegisterDecorator<INewIssueTicket, NewIssueTicketStartedProjectionValidation>();
            container.RegisterDecorator<INewIssueTicket, NewIssueTicketFinishedProjectionValidation>();
            container.RegisterDecorator<INewIssueTicket, NewIssueTicketReservationsCancelValidation>();
            container.RegisterDecorator<INewIssueTicket, NewIssueTicketNonExistingValidation>();

            container.Register<ISeatsAvailability, SeatsAvailability>();
            container.RegisterDecorator<ISeatsAvailability, SeatsAvailabilityStartedProjectionValidation>();
            container.RegisterDecorator<ISeatsAvailability, SeatsAvailabilityFinishedProjectionValidation>();
            container.RegisterDecorator<ISeatsAvailability, SeatsAvailabilityReservationsCancelValidation>();
            container.RegisterDecorator<ISeatsAvailability, SeatsAvailabilityProjectionNonExistingValidation>();

            container.Register<INewRoom, NewRoomCreation>();
            container.RegisterDecorator<INewRoom, NewRoomExistingRoomValidation>();
            container.RegisterDecorator<INewRoom, NewRoomSeatsValidation>();
            container.RegisterDecorator<INewRoom, NewRoomNonExistingCinemaValidation>();
        }
    }
}