﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemAPI.Models.Contracts.Enums
{
    public enum ReservationStatus
    {
        Active = 1,
        Sold = 2,
        Canceled = 3
    }
}
