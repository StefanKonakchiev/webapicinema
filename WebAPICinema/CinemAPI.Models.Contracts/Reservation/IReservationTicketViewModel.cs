﻿using System;

namespace CinemAPI.Models.Contracts.Reservation
{
    public interface IReservationTicketViewModel
    {
        long Id { get; }

        DateTime StartDate { get; }

        string MovieName { get; }

        string CinemaName { get; }

        int RoomNumber { get; }

        short Row { get; }

        short Column { get; }
    }
}
