﻿using CinemAPI.Models.Contracts.Enums;

namespace CinemAPI.Models.Contracts.Reservation
{
    public interface IReservationTicket
    {
        long Id { get; }

        long ProjectionId { get; }

        short Row { get; }

        short Column { get; }

        ReservationStatus Status { get; }
    }
}
