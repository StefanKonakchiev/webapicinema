﻿using CinemAPI.Domain.Contracts.Models;
using System.Threading.Tasks;

namespace CinemAPI.Domain.Contracts
{
    public interface ISeatsAvailability
    {
        Task<SeatsAvailabilitySummary> CheckAvailability(long projectionId);
    }
}
