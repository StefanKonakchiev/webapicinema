﻿namespace CinemAPI.Domain.Contracts.Models
{
    public class SeatsAvailabilitySummary
    {
        public SeatsAvailabilitySummary(bool isValidated)
        {
            this.IsValidated = isValidated;
        }

        public SeatsAvailabilitySummary(bool status, string msg)
            : this(status)
        {
            this.Message = msg;
        }

        public string Message { get; set; }

        public bool IsValidated { get; set; }
    }
}
