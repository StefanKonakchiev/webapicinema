﻿namespace CinemAPI.Domain.Contracts.Models
{
    public class NewRoomSummary
    {
        public NewRoomSummary(bool isCreated)
        {
            this.IsCreated = isCreated;
        }

        public NewRoomSummary(bool status, string msg)
            : this(status)
        {
            this.Message = msg;
        }

        public string Message { get; set; }

        public bool IsCreated { get; set; }
    }
}
