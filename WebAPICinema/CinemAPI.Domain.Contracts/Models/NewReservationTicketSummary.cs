﻿namespace CinemAPI.Domain.Contracts.Models
{
    public class NewReservationTicketSummary
    {
        public NewReservationTicketSummary(bool isCreated)
        {
            this.IsCreated = isCreated;
        }

        public NewReservationTicketSummary(bool status, string msg)
            : this(status)
        {
            this.Message = msg;
        }

        public string Message { get; set; }

        public bool IsCreated { get; set; }
    }
}
