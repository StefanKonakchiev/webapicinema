﻿namespace CinemAPI.Domain.Contracts.Models
{
    public class NewIssueTicketSummary
    {
        public NewIssueTicketSummary(bool isCreated)
        {
            this.IsCreated = isCreated;
        }

        public NewIssueTicketSummary(bool status, string msg)
            : this(status)
        {
            this.Message = msg;
        }

        public string Message { get; set; }

        public bool IsCreated { get; set; }
    }
}
