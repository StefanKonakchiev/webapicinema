﻿using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Room;

namespace CinemAPI.Domain.Contracts
{
    public interface INewRoom
    {
        NewRoomSummary New(IRoomCreation projection);
    }
}
