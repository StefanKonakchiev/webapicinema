﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemAPI.Models.Input.ReservationTicket
{
    public class ReservationTicketCreationModel
    {
        public long ProjectionId { get; set; }

        public short Row { get; set; }

        public short Column { get; set; }
    }
}