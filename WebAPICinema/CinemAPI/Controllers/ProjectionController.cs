﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models;
using CinemAPI.Models.Contracts.Projection;
using CinemAPI.Models.Input.Projection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CinemAPI.Controllers
{
    public class ProjectionController : ApiController
    {
        private readonly INewProjection newProj;
        private readonly ISeatsAvailability seatsAvailability;
        private readonly IProjectionRepository projectRepo;

        public ProjectionController(
            INewProjection newProj,
            ISeatsAvailability seatsAvailability,
            IProjectionRepository projectRepo)
        {
            this.newProj = newProj;
            this.seatsAvailability = seatsAvailability;
            this.projectRepo = projectRepo;
        }

        [HttpPost]
        public IHttpActionResult Index(ProjectionCreationModel model)
        {
            NewProjectionSummary summary = newProj.New(new Projection(model.MovieId, model.RoomId, model.StartDate));

            if (summary.IsCreated)
            {
                return Ok();
            }
            else
            {
                return BadRequest(summary.Message);
            }
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpGet]
        [Route("Api/Projection/SeatsAvailability/{projectionId}")]
        public async Task<IHttpActionResult> SeatsAvailability(long projectionId)
        {
            SeatsAvailabilitySummary summary = await seatsAvailability.CheckAvailability(projectionId);

            if (summary.IsValidated)
            {
                IProjection projection = await projectRepo.Get(projectionId);
                return Ok(projection.AvailableSeatsCount);
            }
            else
            {
                return BadRequest(summary.Message);
            }
        }
    }
}