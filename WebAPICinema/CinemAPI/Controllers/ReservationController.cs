﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models;
using CinemAPI.Models.Contracts.Enums;
using CinemAPI.Models.Contracts.Reservation;
using CinemAPI.Models.Input.ReservationTicket;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CinemAPI.Controllers
{
    public class ReservationController : ApiController
    {
        private readonly INewReservationTicket newReservationTicket;
        private readonly INewIssueTicket newIssueTicket;
        private readonly IReservationTicketRepository reservationTicketsRepo;

        public ReservationController(INewReservationTicket newReservationTicket,
            INewIssueTicket newIssueTicket,
            IReservationTicketRepository reservationTicketsRepo)
        {
            this.newReservationTicket = newReservationTicket;
            this.newIssueTicket = newIssueTicket;
            this.reservationTicketsRepo = reservationTicketsRepo;
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        public async Task<IHttpActionResult> Index(ReservationTicketCreationModel model)
        {
            NewReservationTicketSummary summary = await newReservationTicket
                .New(new ReservationTicket(model.ProjectionId, model.Row, model.Column, ReservationStatus.Active));

            if (summary.IsCreated)
            {
                IReservationTicketViewModel reservationTicket = await reservationTicketsRepo
                    .Get(model.ProjectionId, model.Row, model.Column);
                return Ok(reservationTicket);
            }
            else
            {
                return BadRequest(summary.Message);
            }
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        [Route("Api/Reservation/IssueTicket/{reservationId}")]
        public async Task<IHttpActionResult> IssueTicket(long reservationId)
        {
            NewIssueTicketSummary summary = await newIssueTicket
                .Issue(reservationId);

            if (summary.IsCreated)
            {
                IReservationTicketViewModel reservationTicket = await reservationTicketsRepo
                    .Get(reservationId);
                return Ok(reservationTicket);
            }
            else
            {
                return BadRequest(summary.Message);
            }
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        public async Task<IHttpActionResult> IssueTicket(ReservationTicketCreationModel model)
        {
            NewReservationTicketSummary summary = await newReservationTicket
                .New(new ReservationTicket(model.ProjectionId, model.Row, model.Column, ReservationStatus.Sold));

            if (summary.IsCreated)
            {
                IReservationTicketViewModel reservationTicket = await reservationTicketsRepo
                    .Get(model.ProjectionId, model.Row, model.Column);
                return Ok(reservationTicket);
            }
            else
            {
                return BadRequest(summary.Message);
            }
        }
    }
}