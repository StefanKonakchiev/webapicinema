﻿using CinemAPI.Models.Contracts.Reservation;
using System;

namespace CinemAPI.Models.ViewModels.ReservationTicket
{
    public class ReservationTicketViewModel : IReservationTicketViewModel
    {
        public ReservationTicketViewModel(long id, 
            DateTime startDate, 
            string movieName, 
            string cinemaName, 
            int roomNumber,
            short row,
            short column)
        {
            this.Id = id;
            this.StartDate = startDate;
            this.MovieName = movieName;
            this.CinemaName = cinemaName;
            this.RoomNumber = roomNumber;
            this.Row = row;
            this.Column = column;
        }

        public long Id { get; set; }

        public DateTime StartDate { get; set; }

        public string MovieName { get; set; }

        public string CinemaName { get; set; }

        public int RoomNumber { get; set; }

        public short Row { get; set; }

        public short Column { get; set; }

    }
}