﻿using CinemAPI.Models.Contracts.Projection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CinemAPI.Models
{
    public class Projection : IProjection, IProjectionCreation
    {
        public Projection()
        {
            this.ReservationTickets = new List<ReservationTicket>();
        }

        public Projection(int movieId, int roomId, DateTime startdate)
        {
            this.MovieId = movieId;
            this.RoomId = roomId;
            this.StartDate = startdate;
        }

        public long Id { get; set; }

        public int RoomId { get; set; }

        public virtual Room Room { get; set; }

        public int MovieId { get; set; }
        
        public virtual Movie Movie { get; set; }

        public DateTime StartDate { get; set; }

        [Range(0, 2147483647)]
        public int AvailableSeatsCount { get; set; }

        public virtual ICollection<ReservationTicket> ReservationTickets { get; set; }
    }
}