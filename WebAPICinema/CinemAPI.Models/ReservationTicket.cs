﻿using CinemAPI.Models.Contracts.Enums;
using CinemAPI.Models.Contracts.Reservation;

namespace CinemAPI.Models
{
    public class ReservationTicket : IReservationTicket, IReservationTicketCreation
    {
        public ReservationTicket()
        {

        }

        public ReservationTicket(long projectionId, short row, short column, ReservationStatus status)
        {
            this.ProjectionId = projectionId;
            this.Row = row;
            this.Column = column;
            this.Status = status;
        }

        public long Id { get; set; }

        public long ProjectionId { get; set; }

        public virtual Projection Projection { get; set; }

        public short Row { get; set; }

        public short Column { get; set; }

        public ReservationStatus Status { get; set; }
    }
}
