﻿using CinemAPI.Data.EF;
using CinemAPI.Models;
using CinemAPI.Models.Contracts.Enums;
using CinemAPI.Models.Contracts.Projection;
using CinemAPI.Models.Contracts.Room;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace CinemAPI.Data.Implementation
{
    public class ProjectionRepository : IProjectionRepository
    {
        private readonly CinemaDbContext db;

        public ProjectionRepository(CinemaDbContext db)
        {
            this.db = db;
        }

        public async Task CancelActiveReservations(long projectId)
        {
            List<ReservationTicket> reservations = await db.ReservationTickets
                .Where(x => x.ProjectionId == projectId && x.Status == ReservationStatus.Active).ToListAsync();
            foreach (var reservation in reservations)
            {
                reservation.Status = ReservationStatus.Canceled;
                await this.IncreaseSeatAvailability(projectId);
                db.ReservationTickets.AddOrUpdate(reservation);
                await db.SaveChangesAsync();
            }
        }

        public async Task<bool> CheckIfProjectionHasFinished(long projectId)
        {
            Projection projection = await db.Projections
                .Include(x => x.Movie)
                .SingleOrDefaultAsync(x => x.Id == projectId);
            DateTime now = DateTime.UtcNow;

            if (now > projection.StartDate.AddMinutes(projection.Movie.DurationMinutes))
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CheckIfProjectionPriorToStart(long projectId)
        {
            Projection projection = await db.Projections.SingleOrDefaultAsync(x => x.Id == projectId);
            DateTime now = DateTime.UtcNow;

            if (now > projection.StartDate.AddMinutes(-10))
            {
                return true;
            }

            return false;
        }

        public async Task<bool> CheckIfProjectionStarted(long projectId)
        {
            Projection projection = await db.Projections.SingleOrDefaultAsync(x => x.Id == projectId);
            DateTime now = DateTime.UtcNow;

            if (now > projection.StartDate)
            {
                return true;
            }

            return false;
        }

        public async Task DecreaseSeatAvailability(long projectId)
        {
            Projection projection = await db.Projections.SingleOrDefaultAsync(x => x.Id == projectId);
            projection.AvailableSeatsCount--;
            db.Projections.AddOrUpdate(projection);
            await db.SaveChangesAsync();
        }

        public IProjection Get(int movieId, int roomId, DateTime startDate)
        {
            return db.Projections.FirstOrDefault(x => x.MovieId == movieId &&
                                                      x.RoomId == roomId &&
                                                      x.StartDate == startDate);
        }

        public async Task<IProjection> Get(long projectId)
        {
            return await db.Projections.SingleOrDefaultAsync(x => x.Id == projectId);
        }

        public IEnumerable<IProjection> GetActiveProjections(int roomId)
        {
            DateTime now = DateTime.UtcNow;

            return db.Projections.Where(x => x.RoomId == roomId &&
                                             x.StartDate > now);
        }

        public async Task<IProjection> GetByReservation(long reservationId)
        {
            IProjection projection = await db.Projections.SingleOrDefaultAsync(x => x.ReservationTickets.Any(n => n.Id == reservationId));

            return projection;
        }

        public async Task IncreaseSeatAvailability(long projectId)
        {
            Projection projection = await db.Projections.SingleOrDefaultAsync(x => x.Id == projectId);
            projection.AvailableSeatsCount++;
            db.Projections.AddOrUpdate(projection);
            await db.SaveChangesAsync();
        }

        public void Insert(IProjectionCreation proj)
        {
            Projection newProj = new Projection(proj.MovieId, proj.RoomId, proj.StartDate);
            
            IRoom room = db.Rooms.SingleOrDefault(x => x.Id == proj.RoomId);
            newProj.AvailableSeatsCount = room.Rows * room.SeatsPerRow;

            db.Projections.Add(newProj);
            db.SaveChanges();
        }
    }
}