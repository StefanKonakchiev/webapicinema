﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;
using CinemAPI.Data.EF;
using CinemAPI.Models;
using CinemAPI.Models.Contracts.Enums;
using CinemAPI.Models.Contracts.Reservation;
using CinemAPI.Models.ViewModels.ReservationTicket;

namespace CinemAPI.Data.Implementation
{
    public class ReservationTicketRepository : IReservationTicketRepository
    {
        private readonly CinemaDbContext db;

        public ReservationTicketRepository(CinemaDbContext db)
        {
            this.db = db;
        }

        public async Task<IReservationTicketViewModel> Get(long projectionId, short row, short column)
        {
            ReservationTicket reservationTicket = await db.ReservationTickets
                .SingleOrDefaultAsync(x => x.ProjectionId == projectionId && 
                x.Row == row && 
                x.Column == column && 
                (x.Status == ReservationStatus.Active || x.Status == ReservationStatus.Sold));

            Room room = await db.Rooms
                .Include(x => x.Cinema)
                .SingleOrDefaultAsync(x => x.Id == reservationTicket.Projection.RoomId);

            IReservationTicketViewModel reservationTicketViewModel = new ReservationTicketViewModel
            (
                reservationTicket.Id,
                reservationTicket.Projection.StartDate,
                reservationTicket.Projection.Movie.Name,
                room.Cinema.Name,
                room.Number,
                reservationTicket.Row,
                reservationTicket.Column
            );

            return reservationTicketViewModel;
        }

        public async Task<bool> InsertAsync(IReservationTicketCreation reservationTicket)
        {
            ReservationTicket newReservationTicket = new ReservationTicket(
                reservationTicket.ProjectionId,
                reservationTicket.Row,
                reservationTicket.Column,
                reservationTicket.Status);

            db.ReservationTickets.Add(newReservationTicket);
            return await db.SaveChangesAsync() > 0;
        }

        public async Task<IReservationTicketViewModel> Get(long reservationId)
        {
            ReservationTicket reservationTicket = await db.ReservationTickets
                .SingleOrDefaultAsync(x => x.Id == reservationId);

            if (reservationTicket == null)
            {
                return null;
            }

            Projection projection = await db.Projections
                .Include(x => x.Movie)
                .SingleOrDefaultAsync(x => x.Id == reservationTicket.ProjectionId);
            Room room = await db.Rooms
                .Include(x => x.Cinema)
                .SingleOrDefaultAsync(x => x.Id == reservationTicket.Projection.RoomId);

            IReservationTicketViewModel reservationTicketViewModel = new ReservationTicketViewModel
            (
                reservationTicket.Id,
                projection.StartDate,
                projection.Movie.Name,
                room.Cinema.Name,
                room.Number,
                reservationTicket.Row,
                reservationTicket.Column
            );

            return reservationTicketViewModel;
        }

        public async Task<bool> VerifySeatsAvailability(long projectionId, short row, short column)
        {
            IReservationTicket reservation = await db.ReservationTickets
                .SingleOrDefaultAsync(x => x.ProjectionId == projectionId && 
                x.Row == row && 
                x.Column == column && (x.Status == ReservationStatus.Active || x.Status == ReservationStatus.Sold));

            if (reservation == null)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> CheckIfSold(long reservationId)
        {
            ReservationTicket reservationTicket = await db.ReservationTickets
                .SingleOrDefaultAsync(x => x.Id == reservationId && x.Status == ReservationStatus.Sold);

            if (reservationTicket == null)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> UpdateReservationStatus(long reservationId)
        {
            ReservationTicket reservationTicket = await db.ReservationTickets
                .SingleOrDefaultAsync(x => x.Id == reservationId && x.Status == ReservationStatus.Active);
            reservationTicket.Status = ReservationStatus.Sold;

            db.ReservationTickets.AddOrUpdate(reservationTicket);
            return await db.SaveChangesAsync() > 0;
        }
    }
}
