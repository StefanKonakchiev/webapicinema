﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using System.Threading.Tasks;

namespace CinemAPI.Domain.SeatsAvailability
{
    public class SeatsAvailabilityReservationsCancelValidation : ISeatsAvailability
    {
        private readonly IProjectionRepository projectRepo;
        private readonly ISeatsAvailability seatsAvailability;

        public SeatsAvailabilityReservationsCancelValidation(
            IProjectionRepository projectRepo,
            ISeatsAvailability seatsAvailability)
        {
            this.projectRepo = projectRepo;
            this.seatsAvailability = seatsAvailability;
        }
        public async Task<SeatsAvailabilitySummary> CheckAvailability(long projectionId)
        {
            if (await projectRepo.CheckIfProjectionPriorToStart(projectionId))
            {
                await projectRepo.CancelActiveReservations(projectionId);
            }

            return await this.seatsAvailability.CheckAvailability(projectionId);
        }
    }
}
