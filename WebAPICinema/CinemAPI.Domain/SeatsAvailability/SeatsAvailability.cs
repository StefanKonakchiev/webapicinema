﻿using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using System.Threading.Tasks;

namespace CinemAPI.Domain.SeatsAvailability
{
    public class SeatsAvailability : ISeatsAvailability
    {
        public SeatsAvailability()
        {
        }

        public async Task<SeatsAvailabilitySummary> CheckAvailability(long projectionId)
        {
            return new SeatsAvailabilitySummary(true);
        }

    }
}
