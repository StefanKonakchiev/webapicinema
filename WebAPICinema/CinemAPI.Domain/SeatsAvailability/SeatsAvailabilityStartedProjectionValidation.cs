﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using System.Threading.Tasks;

namespace CinemAPI.Domain.SeatsAvailability
{
    public class SeatsAvailabilityStartedProjectionValidation : ISeatsAvailability
    {
        private readonly IProjectionRepository projectRepo;
        private readonly ISeatsAvailability seatsAvailability;

        public SeatsAvailabilityStartedProjectionValidation(
            IProjectionRepository projectRepo,
            ISeatsAvailability seatsAvailability)
        {
            this.projectRepo = projectRepo;
            this.seatsAvailability = seatsAvailability;
        }
        public async Task<SeatsAvailabilitySummary> CheckAvailability(long projectionId)
        {
            if (await projectRepo.CheckIfProjectionStarted(projectionId))
            {
                return new SeatsAvailabilitySummary(false, $"Projection with id {projectionId} has started");
            }

            return await this.seatsAvailability.CheckAvailability(projectionId);
        }
    }
}
