﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Projection;
using System.Threading.Tasks;

namespace CinemAPI.Domain.SeatsAvailability
{
    public class SeatsAvailabilityProjectionNonExistingValidation : ISeatsAvailability
    {
        private readonly IProjectionRepository projectRepo;
        private readonly ISeatsAvailability seatsAvailability;

        public SeatsAvailabilityProjectionNonExistingValidation(
            IProjectionRepository projectRepo, 
            ISeatsAvailability seatsAvailability)
        {
            this.projectRepo = projectRepo;
            this.seatsAvailability = seatsAvailability;
        }
        public async Task<SeatsAvailabilitySummary> CheckAvailability(long projectionId)
        {
            IProjection projection = await projectRepo.Get(projectionId);

            if (projection == null)
            {
                return new SeatsAvailabilitySummary(false, $"Projection with id {projectionId} does not exist");
            }

            return await this.seatsAvailability.CheckAvailability(projectionId);
        }
    }
}
