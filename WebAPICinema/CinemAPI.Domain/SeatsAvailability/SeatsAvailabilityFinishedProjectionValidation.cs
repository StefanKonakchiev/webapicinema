﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using System.Threading.Tasks;

namespace CinemAPI.Domain.SeatsAvailability
{
    public class SeatsAvailabilityFinishedProjectionValidation : ISeatsAvailability
    {
        private readonly IProjectionRepository projectRepo;
        private readonly ISeatsAvailability seatsAvailability;

        public SeatsAvailabilityFinishedProjectionValidation(
            IProjectionRepository projectRepo, 
            ISeatsAvailability seatsAvailability)
        {
            this.projectRepo = projectRepo;
            this.seatsAvailability = seatsAvailability;
        }
        public async Task<SeatsAvailabilitySummary> CheckAvailability(long projectionId)
        {
            if (await projectRepo.CheckIfProjectionHasFinished(projectionId))
            {
                return new SeatsAvailabilitySummary(false, $"Projection with id {projectionId} has finished");
            }

            return await this.seatsAvailability.CheckAvailability(projectionId);
        }
    }
}
