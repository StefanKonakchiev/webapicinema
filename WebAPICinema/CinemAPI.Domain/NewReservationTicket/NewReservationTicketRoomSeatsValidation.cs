﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Projection;
using CinemAPI.Models.Contracts.Reservation;
using CinemAPI.Models.Contracts.Room;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewReservationTicket
{
    public class NewReservationTicketRoomSeatsValidation : INewReservationTicket
    {
        private readonly IRoomRepository roomRepo;
        private readonly IProjectionRepository projectionsRepo;
        private readonly INewReservationTicket newReservationTicket;

        public NewReservationTicketRoomSeatsValidation(
            IRoomRepository roomRepo,
            IProjectionRepository projectionsRepo,
            INewReservationTicket newReservationTicket)
        {
            this.roomRepo = roomRepo;
            this.projectionsRepo = projectionsRepo;
            this.newReservationTicket = newReservationTicket;
        }

        public async Task<NewReservationTicketSummary> New(IReservationTicketCreation reservationTicket)
        {
            IProjection projection = await projectionsRepo.Get(reservationTicket.ProjectionId);
            IRoom room = roomRepo.GetById(projection.RoomId);

            if (reservationTicket.Row > room.Rows || reservationTicket.Row < 1 || 
                reservationTicket.Column > room.SeatsPerRow || reservationTicket.Column < 1)
            {
                return new NewReservationTicketSummary(false, $"Seats do not exist in the room");
            }

            return await this.newReservationTicket.New(reservationTicket);
        }
    }
}
