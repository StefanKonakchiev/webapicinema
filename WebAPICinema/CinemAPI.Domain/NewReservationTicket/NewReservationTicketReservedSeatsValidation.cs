﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Reservation;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewReservationTicket
{
    public class NewReservationTicketReservedSeatsValidation : INewReservationTicket
    {
        private readonly IReservationTicketRepository reservationTicketsRepo;
        private readonly INewReservationTicket newReservationTicket;

        public NewReservationTicketReservedSeatsValidation(
            IReservationTicketRepository reservationTicketsRepo,
            INewReservationTicket newReservationTicket)
        {
            this.reservationTicketsRepo = reservationTicketsRepo;
            this.newReservationTicket = newReservationTicket;
        }

        public async Task<NewReservationTicketSummary> New(IReservationTicketCreation reservationTicket)
        {
            bool reserved = await reservationTicketsRepo
                .VerifySeatsAvailability(reservationTicket.ProjectionId, reservationTicket.Row, reservationTicket.Column);

            if (reserved)
            {
                return new NewReservationTicketSummary(false, $"Requested seats are already reserved");
            }

            return await this.newReservationTicket.New(reservationTicket);
        }
    }
}
