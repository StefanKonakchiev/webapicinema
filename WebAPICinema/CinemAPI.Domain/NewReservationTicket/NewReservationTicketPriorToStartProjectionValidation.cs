﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Enums;
using CinemAPI.Models.Contracts.Reservation;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewReservationTicket
{
    public class NewReservationTicketPriorToStartProjectionValidation : INewReservationTicket
    {
        private readonly IProjectionRepository projectionsRepo;
        private readonly INewReservationTicket newReservationTicket;

        public NewReservationTicketPriorToStartProjectionValidation(
            IProjectionRepository projectionsRepo,
            INewReservationTicket newReservationTicket)
        {
            this.projectionsRepo = projectionsRepo;
            this.newReservationTicket = newReservationTicket;
        }

        public async Task<NewReservationTicketSummary> New(IReservationTicketCreation reservationTicket)
        {
            if (await projectionsRepo.CheckIfProjectionPriorToStart(reservationTicket.ProjectionId) &&
                reservationTicket.Status == ReservationStatus.Active)
            {
                return new NewReservationTicketSummary(false, $"Projection with id {reservationTicket.ProjectionId} is about to start in 10 minutes");
            }

            return await this.newReservationTicket.New(reservationTicket);
        }
    }
}
