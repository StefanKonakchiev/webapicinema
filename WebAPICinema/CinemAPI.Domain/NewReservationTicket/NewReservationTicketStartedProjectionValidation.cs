﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Reservation;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewReservationTicket
{
    public class NewReservationTicketStartedProjectionValidation : INewReservationTicket
    {
        private readonly IProjectionRepository projectionsRepo;
        private readonly INewReservationTicket newReservationTicket;

        public NewReservationTicketStartedProjectionValidation(
            IProjectionRepository projectionsRepo,
            INewReservationTicket newReservationTicket)
        {
            this.projectionsRepo = projectionsRepo;
            this.newReservationTicket = newReservationTicket;
        }

        public async Task<NewReservationTicketSummary> New(IReservationTicketCreation reservationTicket)
        {
            if (await projectionsRepo.CheckIfProjectionStarted(reservationTicket.ProjectionId))
            {
                return new NewReservationTicketSummary(false, $"Projection with id {reservationTicket.ProjectionId} has started");
            }

            return await this.newReservationTicket.New(reservationTicket);
        }
    }
}
