﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Projection;
using CinemAPI.Models.Contracts.Reservation;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewReservationTicket
{
    public class NewReservationTicketProjectionNonExistingValidation : INewReservationTicket
    {
        private readonly IRoomRepository roomRepo;
        private readonly IProjectionRepository projectionsRepo;
        private readonly INewReservationTicket newReservationTicket;

        public NewReservationTicketProjectionNonExistingValidation(
            IRoomRepository roomRepo,
            IProjectionRepository projectionsRepo,
            INewReservationTicket newReservationTicket)
        {
            this.roomRepo = roomRepo;
            this.projectionsRepo = projectionsRepo;
            this.newReservationTicket = newReservationTicket;
        }

        public async Task<NewReservationTicketSummary> New(IReservationTicketCreation reservationTicket)
        {
            IProjection projection = await projectionsRepo.Get(reservationTicket.ProjectionId);

            if (projection == null)
            {
                return new NewReservationTicketSummary(false, $"Projection with id {reservationTicket.ProjectionId} does not exist");
            }

            return await this.newReservationTicket.New(reservationTicket);
        }
    }
}
