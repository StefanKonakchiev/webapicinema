﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models;
using CinemAPI.Models.Contracts.Reservation;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewReservationTicket
{
    public class NewReservationTicketCreation : INewReservationTicket
    {
        private readonly IReservationTicketRepository reservationTicketsRepo;
        private readonly IProjectionRepository projectionsRepo;

        public NewReservationTicketCreation(
            IReservationTicketRepository reservationTicketsRepo,
            IProjectionRepository projectionsRepo)
        {
            this.reservationTicketsRepo = reservationTicketsRepo;
            this.projectionsRepo = projectionsRepo;
        }

        public async Task<NewReservationTicketSummary> New(IReservationTicketCreation reservationTicket)
        {
            bool result = await reservationTicketsRepo.InsertAsync(new ReservationTicket(
                reservationTicket.ProjectionId,
                reservationTicket.Row,
                reservationTicket.Column,
                reservationTicket.Status));

            if (result)
            {
                await projectionsRepo.DecreaseSeatAvailability(reservationTicket.ProjectionId);

                return new NewReservationTicketSummary(true);
            }

            return new NewReservationTicketSummary(false);
        }
    }
}
