﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Reservation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewReservationTicket
{
    public class NewReservationTicketReservationsCancelValidation : INewReservationTicket
    {
        private readonly IProjectionRepository projectionsRepo;
        private readonly INewReservationTicket newReservationTicket;

        public NewReservationTicketReservationsCancelValidation(
            IProjectionRepository projectionsRepo,
            INewReservationTicket newReservationTicket)
        {
            this.projectionsRepo = projectionsRepo;
            this.newReservationTicket = newReservationTicket;
        }

        public async Task<NewReservationTicketSummary> New(IReservationTicketCreation reservationTicket)
        {
            if (await projectionsRepo.CheckIfProjectionPriorToStart(reservationTicket.ProjectionId))
            {
                await projectionsRepo.CancelActiveReservations(reservationTicket.ProjectionId);
            }

            return await this.newReservationTicket.New(reservationTicket);
        }
    }
}
