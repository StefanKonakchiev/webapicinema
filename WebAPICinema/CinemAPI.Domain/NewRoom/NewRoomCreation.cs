﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models;
using CinemAPI.Models.Contracts.Room;

namespace CinemAPI.Domain.NewRoom
{
    public class NewRoomCreation : INewRoom
    {
        private readonly IRoomRepository roomRepo;

        public NewRoomCreation(IRoomRepository roomRepo)
        {
            this.roomRepo = roomRepo;
        }
        
        public NewRoomSummary New(IRoomCreation room)
        {
            roomRepo.Insert(new Room(room.Number, room.SeatsPerRow, room.Rows, room.CinemaId));

            return new NewRoomSummary(true);
        }
    }
}
