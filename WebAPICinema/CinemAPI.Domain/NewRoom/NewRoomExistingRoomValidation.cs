﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Room;

namespace CinemAPI.Domain.NewRoom
{
    public class NewRoomExistingRoomValidation : INewRoom
    {
        private readonly IRoomRepository roomRepo;
        private readonly INewRoom newRoom;

        public NewRoomExistingRoomValidation(IRoomRepository roomRepo, INewRoom newRoom)
        {
            this.roomRepo = roomRepo;
            this.newRoom = newRoom;
        }

        public NewRoomSummary New(IRoomCreation room)
        {
            IRoom roomByCinemaAndNumber = roomRepo.GetByCinemaAndNumber(room.CinemaId, room.Number);

            if (roomByCinemaAndNumber != null)
            {
                return new NewRoomSummary(false, "Room already exists");
            }

            return newRoom.New(room);
        }
    }
}
