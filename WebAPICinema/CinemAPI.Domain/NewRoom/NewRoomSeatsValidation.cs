﻿using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Room;

namespace CinemAPI.Domain.NewRoom
{
    public class NewRoomSeatsValidation : INewRoom
    {
        private readonly INewRoom newRoom;

        public NewRoomSeatsValidation(INewRoom newRoom)
        {
            this.newRoom = newRoom;
        }

        public NewRoomSummary New(IRoomCreation room)
        {
            if (room.SeatsPerRow * room.Rows < 0)
            {
                return new NewRoomSummary(false, "The room cannot have negative seat values");
            }

            return newRoom.New(room);
        }
    }
}
