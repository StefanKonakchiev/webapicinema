﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Cinema;
using CinemAPI.Models.Contracts.Room;

namespace CinemAPI.Domain.NewRoom
{
    public class NewRoomNonExistingCinemaValidation : INewRoom
    {
        private readonly ICinemaRepository cinemaRepo;
        private readonly INewRoom newRoom;

        public NewRoomNonExistingCinemaValidation(ICinemaRepository cinemaRepo, INewRoom newRoom)
        {
            this.cinemaRepo = cinemaRepo;
            this.newRoom = newRoom;
        }

        public NewRoomSummary New(IRoomCreation room)
        {
            ICinema cinema = cinemaRepo.GetById(room.CinemaId);

            if (cinema == null)
            {
                return new NewRoomSummary(false, $"Cinema with id {room.CinemaId} does not exist");
            }

            return newRoom.New(room);
        }
    }
}
