﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Projection;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewIssueTicket
{
    public class NewIssueTicketStartedProjectionValidation : INewIssueTicket
    {
        private readonly IProjectionRepository projectRepo;
        private readonly INewIssueTicket newIssueTicket;

        public NewIssueTicketStartedProjectionValidation(
            IProjectionRepository projectRepo,
            INewIssueTicket newIssueTicket)
        {
            this.projectRepo = projectRepo;
            this.newIssueTicket = newIssueTicket;
        }

        public async Task<NewIssueTicketSummary> Issue(long reservationId)
        {
            IProjection projection = await projectRepo.GetByReservation(reservationId);

            if (await projectRepo.CheckIfProjectionStarted(projection.Id))
            {
                return new NewIssueTicketSummary(false, $"Projection with id {projection.Id} has started");
            }

            return await this.newIssueTicket.Issue(reservationId);
        }
    }
}
