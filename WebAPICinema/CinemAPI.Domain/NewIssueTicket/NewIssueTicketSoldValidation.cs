﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewIssueTicket
{
    public class NewIssueTicketSoldValidation : INewIssueTicket
    {
        private readonly IReservationTicketRepository reservationTicketsRepo;
        private readonly INewIssueTicket newIssueTicket;

        public NewIssueTicketSoldValidation(
            IReservationTicketRepository reservationTicketsRepo,
            INewIssueTicket newIssueTicket)
        {
            this.reservationTicketsRepo = reservationTicketsRepo;
            this.newIssueTicket = newIssueTicket;
        }

        public async Task<NewIssueTicketSummary> Issue(long reservationId)
        {
            if (await reservationTicketsRepo.CheckIfSold(reservationId))
            {
                return new NewIssueTicketSummary(false, "This reservation has already been sold");
            }

            return await this.newIssueTicket.Issue(reservationId);
        }
    }
}
