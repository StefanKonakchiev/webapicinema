﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Projection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewIssueTicket
{
    public class NewIssueTicketReservationsCancelValidation : INewIssueTicket
    {
        private readonly IProjectionRepository projectRepo;
        private readonly INewIssueTicket newIssueTicket;

        public NewIssueTicketReservationsCancelValidation(
            IProjectionRepository projectRepo,
            INewIssueTicket newIssueTicket)
        {
            this.projectRepo = projectRepo;
            this.newIssueTicket = newIssueTicket;
        }

        public async Task<NewIssueTicketSummary> Issue(long reservationId)
        {
            IProjection projection = await projectRepo.GetByReservation(reservationId);

            if (await projectRepo.CheckIfProjectionPriorToStart(projection.Id))
            {
                await projectRepo.CancelActiveReservations(projection.Id);
            }

            return await this.newIssueTicket.Issue(reservationId);
        }
    }
}
