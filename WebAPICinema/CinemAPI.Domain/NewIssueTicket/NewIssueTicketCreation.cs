﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewIssueTicket
{
    public class NewIssueTicketCreation : INewIssueTicket
    {
        private readonly IReservationTicketRepository reservationTicketsRepo;

        public NewIssueTicketCreation(
            IReservationTicketRepository reservationTicketsRepo)
        {
            this.reservationTicketsRepo = reservationTicketsRepo;
        }

        public async Task<NewIssueTicketSummary> Issue(long reservationId)
        {
            bool result = await reservationTicketsRepo.UpdateReservationStatus(reservationId);
            
            if (result)
            {
                return new NewIssueTicketSummary(true);
            }

            return new NewIssueTicketSummary(false);
        }
    }
}
