﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Reservation;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewIssueTicket
{
    public class NewIssueTicketNonExistingValidation : INewIssueTicket
    {
        private readonly IReservationTicketRepository reservationTicketsRepo;
        private readonly INewIssueTicket newIssueTicket;

        public NewIssueTicketNonExistingValidation(
            IReservationTicketRepository reservationTicketsRepo,
            INewIssueTicket newIssueTicket)
        {
            this.reservationTicketsRepo = reservationTicketsRepo;
            this.newIssueTicket = newIssueTicket;
        }

        public async Task<NewIssueTicketSummary> Issue(long reservationId)
        {
            IReservationTicketViewModel reservationTicket = await reservationTicketsRepo
                    .Get(reservationId);

            if (reservationTicket == null)
            {
                return new NewIssueTicketSummary(false, "Reservation does not exist");
            }

            return await this.newIssueTicket.Issue(reservationId);
        }
    }
}
