﻿using CinemAPI.Data;
using CinemAPI.Domain.Contracts;
using CinemAPI.Domain.Contracts.Models;
using CinemAPI.Models.Contracts.Projection;
using System.Threading.Tasks;

namespace CinemAPI.Domain.NewIssueTicket
{
    public class NewIssueTicketPriorToStartProjectionValidation : INewIssueTicket
    {
        private readonly IProjectionRepository projectRepo;
        private readonly INewIssueTicket newIssueTicket;

        public NewIssueTicketPriorToStartProjectionValidation(
            IProjectionRepository projectRepo,
            INewIssueTicket newIssueTicket)
        {
            this.projectRepo = projectRepo;
            this.newIssueTicket = newIssueTicket;
        }

        public async Task<NewIssueTicketSummary> Issue(long reservationId)
        {
            IProjection projection = await projectRepo.GetByReservation(reservationId);

            if (await projectRepo.CheckIfProjectionPriorToStart(projection.Id))
            {
                return new NewIssueTicketSummary(false, "Projection is about to start in 10 minutes and all reservations have been canceled");
            }

            return await this.newIssueTicket.Issue(reservationId);
        }
    }
}
