const appUrl = 'http://localhost:50593/api/';

function getAvailableSeats() {
    let projectionId = $('#projectionId').val();

    if (projectionId == 0) {
        renderMessages('Projection Id cannot be empty');
        return;
    }

    fetch(appUrl + 'projection/seatsavailability/' + projectionId).then(function(response) {
        if (response.status === 400) {
            response.json().then(function(object) {
                renderMessages(object.Message)
        })
        } else if (response.status === 200) {
            response.json().then(function(object) {
                renderMessages(object)
            }) 
        }
    })
}

function renderMessages(data) {
    $('#messages').empty();

        $('#messages')
            .append('<div class="message d-flex justify-content-start"><strong>'
                + data
                +'</strong></div>');

}

function renderResult(data) {
    $('#messages').empty();

    Object.keys(data).forEach(e => { 
        $('#messages')
            .append('<div class="message d-flex justify-content-start"><strong>'
                + e
                +'</strong>: ' + data[e] + '</div>')
    });
}

function reservationCreation() {

    let projectionId = $('#projectionId-reservation').val();
    let row = $('#row-reservation').val();
    let column = $('#column-reservation').val();

    if (projectionId == 0) {
        renderMessages('Projection Id cannot be empty');
        return;
    }

    fetch(appUrl + 'reservation', {
        method: 'post',
        headers:{
                    'Content-Type': 'application/json'
                },
        body: JSON.stringify({ProjectionId: projectionId, Row: row, Column: column})
    }).then(function(response) {
        if (response.status === 400) {
          response.json().then(function(object) {
            renderMessages(object.Message)
          })
        } else if (response.status === 200) {
          response.json().then(function(object) {
            renderResult(object);
          })
        }
      })
}

function ticketCreation() {

    let projectionId = $('#projectionId-ticket').val();
    let row = $('#row-ticket').val();
    let column = $('#column-ticket').val();

    if (projectionId == 0) {
        renderMessages('Projection Id cannot be empty');
        return;
    }

    fetch(appUrl + 'reservation/issueticket', {
        method: 'post',
        headers:{
                    'Content-Type': 'application/json'
                },
        body: JSON.stringify({ProjectionId: projectionId, Row: row, Column: column})
    }).then(function(response) {
        if (response.status === 400) {
          response.json().then(function(object) {
            renderMessages(object.Message)
          })
        } else if (response.status === 200) {
          response.json().then(function(object) {
            renderResult(object);
          })
        }
      })
}

function ticketCreationByReservation() {

    let reservationId = $('#reservationId-ticket').val();
    let row = $('#row-ticket').val();
    let column = $('#column-ticket').val();

    if (reservationId == 0) {
        renderMessages('Projection Id cannot be empty');
        return;
    }

    fetch(appUrl + 'reservation/issueticket/' + reservationId, {
        method: 'post',
    }).then(function(response) {
        if (response.status === 400) {
          response.json().then(function(object) {
            renderMessages(object.Message)
          })
        } else if (response.status === 200) {
          response.json().then(function(object) {
            renderResult(object);
          })
        }
      })
}
